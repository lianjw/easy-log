package com.idea.easy.log.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.idea.easy.log.constant.Constant;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * @className: ApiLog
 * @description:
 * @author: salad
 * @date: 2022/7/1
 **/

@Data
@ToString(callSuper = true)
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@TableName(Constant.API_LOG_TABLE_NAME)
public class ApiLog extends ApiLogModel{

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

}
