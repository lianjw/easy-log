package com.idea.easy.log.config;

import com.idea.easy.log.aspect.ApiLogAspect;
import com.idea.easy.log.listener.ApiLogListener;
import com.idea.easy.log.listener.CustomLogListener;
import com.idea.easy.log.listener.ErrorLogListener;
import com.idea.easy.log.logger.EasyLogger;
import com.idea.easy.log.props.EasyLogProperties;
import com.idea.easy.log.provider.EasyLogServerInfoProvider;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @className: EasyLogAutoConfiguration
 * @description: EasyLog的自动配置类
 * @author: salad
 * @date: 2022/6/9
 **/
@EnableAsync
@Configuration(
        proxyBeanMethods = false
)
@ConditionalOnWebApplication
@ConditionalOnProperty(
        value = {"easy-log.enabled"},
        havingValue = "true"
)
@EnableConfigurationProperties({EasyLogProperties.class})
public class EasyLogAutoConfiguration {

    @Bean
    public ApiLogAspect apiLogAspect() {
        return new ApiLogAspect();
    }

    @Bean
    public EasyLogger easyLogger() {
        return new EasyLogger();
    }

    @Bean
    @ConditionalOnMissingBean(
            name = {"apiLogListener"}
    )
    public ApiLogListener apiLogListener(EasyLogProperties properties, EasyLogServerInfoProvider provider) {
        return new ApiLogListener(properties, provider);
    }

    @Bean
    @ConditionalOnMissingBean(
            name = {"errorLogListener"}
    )
    public ErrorLogListener errorLogListener(EasyLogProperties properties, EasyLogServerInfoProvider provider) {
        return new ErrorLogListener(properties, provider);
    }

    @Bean
    @ConditionalOnMissingBean(
            name = {"customLogListener"}
    )
    public CustomLogListener customLogListener(EasyLogProperties properties, EasyLogServerInfoProvider provider) {
        return new CustomLogListener(properties, provider);
    }

    @Bean
    @ConditionalOnMissingBean(
            name = {"easyLogBanner"}
    )
    @ConditionalOnProperty(value = {"easy-log.banner"},havingValue = "true",matchIfMissing = true)
    public EasyLogBanner easyLogBanner() {
        return new EasyLogBanner();
    }


}