package com.idea.easy.log.logger;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @className: LogLevel
 * @description: 自定义日志的级别
 * @author: salad
 * @date: 2022/6/8
 **/
@Getter
@AllArgsConstructor
public enum LogLevel {

    INFO("info"),

    DEBUG("debug"),

    WARN("warn"),

    ERROR("error");

    private final String level;

}
