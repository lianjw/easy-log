package com.idea.easy.log.logger;

import com.idea.easy.log.event.CustomLogEvent;
import com.idea.easy.log.model.CustomLogModel;
import com.idea.easy.log.publisher.CustomLogPublisher;

/**
 * @className: EasyLogger
 * @description: 自定义日志的方法
 * @author: salad
 * @date: 2022/6/8
 **/
public class EasyLogger {

    /**
     * info级别日志
     * @param logId 日志标识
     * @param logData 要记录的日志数据
     */
    public void info(String logId,String logData,Object ...args){
        log(LogLevel.INFO,logId,logData,args);
    }

    /**
     * debug级别日志
     * @param logId 日志标识
     * @param logData 要记录的日志数据
     */
    public void debug(String logId,String logData,Object ...args){
        log(LogLevel.DEBUG,logId,logData,args);
    }

    /**
     * warn级别日志
     * @param logId 日志标识
     * @param logData 要记录的日志数据
     */
    public void warn(String logId,String logData,Object ...args){
        log(LogLevel.WARN,logId,logData,args);
    }

    /**
     * error级别日志
     * @param logId 日志标识
     * @param logData 要记录的日志数据
     */
    public void error(String logId,String logData,Object ...args){
       log(LogLevel.ERROR,logId,logData,args);
    }

    /**
     * 可指定级别的日志
     * @param logLevel 日志等级
     * @param logId 日志标识
     * @param logData 要记录的日志数据
     */
    public void log(LogLevel logLevel,String logId,String logData,Object ...args){
        CustomLogEvent event = new CustomLogEvent(new CustomLogModel());
        CustomLogPublisher
                .event(event,logId,String.format(logData,args),logLevel.getLevel()).publish();

    }


}
