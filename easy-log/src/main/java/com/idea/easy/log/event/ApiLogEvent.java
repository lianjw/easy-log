package com.idea.easy.log.event;

import com.idea.easy.log.model.ApiLogModel;
import org.springframework.context.ApplicationEvent;

/**
 * @className: ApiLogEvent
 * @description: ApiLog事件
 * @author: salad
 * @date: 2022/6/9
 **/
public class ApiLogEvent extends ApplicationEvent {

	public ApiLogEvent(ApiLogModel source) {
		super(source);
	}

}
