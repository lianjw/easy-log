package com.idea.easy.log.config;

import lombok.extern.slf4j.Slf4j;

/**
 * @className: EasyLogBanner
 * @description:
 * @author: salad
 * @date: 2022/6/16
 **/
public class EasyLogBanner {


    public EasyLogBanner(){
        printLogo();
    }

    private void printLogo(){
        System.out.println("\n" +
                "  ______                  _                 \n" +
                " |  ____|                | |                \n" +
                " | |__   __ _ ___ _   _  | |     ___   __ _ \n" +
                " |  __| / _` / __| | | | | |    / _ \\ / _` |\n" +
                " | |___| (_| \\__ \\ |_| | | |___| (_) | (_| |\n" +
                " |______\\__,_|___/\\__, | |______\\___/ \\__, |\n" +
                "                   __/ |               __/ |\n" +
                "                  |___/               |___/ \n" +
                "                                           By Idea\n");
    }

}
