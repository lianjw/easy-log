package com.idea.easy.log.utils;

import org.springframework.util.Assert;

import java.util.IdentityHashMap;
import java.util.Map;

/**
 * @className: ClassUtil
 * @description:
 * @author: salad
 * @date: 2022/6/10
 **/
public class ClassUtil {

    private static final Map<Class<?>, Class<?>> primitiveWrapperTypeMap =
            new IdentityHashMap<Class<?>, Class<?>>(8);

    public static boolean isPrimitiveOrWrapper(Class<?> clazz) {
        Assert.notNull(clazz, "Class must not be null");
        return clazz.isPrimitive() || isPrimitiveWrapper(clazz);
    }

    public static boolean isPrimitiveWrapper(Class<?> clazz) {
        Assert.notNull(clazz, "Class must not be null");
        return primitiveWrapperTypeMap.containsKey(clazz);
    }

}
