package com.idea.easy.log.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.idea.easy.log.model.CustomLog;

/**
 * @className: ICustomLogService
 * @description: 提供持久化日志方法的接口
 * @author: salad
 * @date: 2022/6/4
 **/
public interface ICustomLogService extends IService<CustomLog> {

  void saveCustomLog(CustomLog customLog);

}
