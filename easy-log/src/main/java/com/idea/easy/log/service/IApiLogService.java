package com.idea.easy.log.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.idea.easy.log.model.ApiLog;
import com.idea.easy.log.model.ApiLogModel;
import com.idea.easy.log.model.CustomLogModel;
import com.idea.easy.log.model.ErrorLogModel;

/**
 * @className: IApiLogService
 * @description: 提供持久化日志方法的接口
 * @author: salad
 * @date: 2022/6/4
 **/
public interface IApiLogService extends IService<ApiLog> {

  void saveApiLog(ApiLog apiLog);

}
